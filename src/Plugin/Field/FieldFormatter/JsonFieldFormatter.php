<?php

/**
 * @file
 * Contains \Drupal\json_field_formatter\Plugin\Field\FieldFormatter\JsonFieldFormatter.
 */

namespace Drupal\json_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'json_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "json_field_formatter",
 *   label = @Translation("JSON Field Formatter"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "string",
 *     "string_long"
 *   }
 * )
 */
class JsonFieldFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $json = $item->value;
      $decoded = json_decode($json);
      $pretty_json = json_encode($decoded, JSON_PRETTY_PRINT);
      $elements[$delta] = ['#markup' => "<pre><code class='javascript'>$pretty_json</code></pre>"];
      $elements['#attached']['library'][] = 'json_field_formatter/highlight-js';
    }

    return $elements;
  }

}
