(function () {

  'use strict';

  function init() {
    hljs.initHighlightingOnLoad();
  }

  if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', init);
  }

})();
